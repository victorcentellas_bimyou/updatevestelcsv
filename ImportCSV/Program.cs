﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using ImportCSV.Slack;
using ZendeskApi_v2;
using ZendeskApi_v2.Models.Tickets;

namespace ImportCSV
{
    class Program
    {

        #region Atributos
        private static ZendeskApi _api;
        #endregion

        #region Api()
        private static ZendeskApi Api()
        {
            try
            {
                if (_api == null)
                {
                    _api = new ZendeskApi("https://vesteliberia.zendesk.com", "8b0f8b026af31f5afbacd073d2b62b755bf5a699d470bd7b8215f0d3fbbd0799");
                }
            }
            catch
            {
                _api = null;
            }
            return _api;
        }
        #endregion
        #region Helpers
        /// <summary>
        /// Get the correct format for the material code
        /// </summary>
        /// <param name="materialCode">Material code without the proper format.</param>
        /// <returns>The material code formated.</returns>
        static private string GetFormat(string materialCode)
        {
            // Material code must be 18 characters long
            while (materialCode.Length < 18)
            {
                materialCode = "0" + materialCode;
            }

            return materialCode;
        }

        /// <summary>
        /// Cleans the string from " char.
        /// </summary>
        /// <param name="name">String to be cleaned</param>
        /// <returns>The string without multiple "</returns>
        static private string GetNameClean(string name)
        {
            // Check if the word contains "
            string newName = name;
            int numChar = name.Count(f => f == '"');
            if (numChar > 1)
            {
                newName = "";
                // Deletes the " except if the " is just after a number
                for (int i =0; i<name.Count();i++)
                {
                    if (name[i] != '"')
                    {
                        newName = newName + name[i];
                    }
                    else
                    {
                        if (i != 0)
                        {
                            if(Char.IsDigit(name[i-1]))
                            {
                                newName = newName + name[i];
                            }
                        }
                    }
                
                }
            }
            // Delete the º symbol
            return newName;
        }
        #endregion

        #region GetDictWithCFOptions
        /// <summary>
        /// Create a dictionary containing all the custom field options. The key is the custom field value and
        /// the value is the custom field name.
        /// </summary>
        /// <param name="cfOptions">All options for a custom field </param>
        /// <returns>Dictionary with all the custom field options.</returns>
        private static Dictionary<string, string> GetDictWithCFOptions(IList<CustomFieldOptions> cfOptions)
        {
            Dictionary<string, string> info = new Dictionary<string, string>();
            foreach (CustomFieldOptions cf in cfOptions)
            {
                info.Add(cf.Value, cf.Name);
            }
            return info;
        }


        #endregion


        #region GenNewCSV()
        /// <summary>
        /// Generate a new CSV file with the fields formated ready to be imported.
        /// </summary>
        /// <param name="pathFile">Path to the CSV file to extract the information from.</param>
        /// <param name="columna">Column to extract the information from. The first one is 0 and the last one 7.</param>
        /// <returns>Path of the new generated file.</returns>
        static private string  GenNewCSV(string pathFile ,int columna, string prefijo, string sufijo)
        {
            string delimiter = ";";
            string savedPath = @"C:\Users\vcent\Projects\UpadeVestel\final_" + $"{columna}.csv";
            StringBuilder sb = new StringBuilder();

            using (var read = new StreamReader(pathFile))
            {
                // The first line of the document contains the header
                read.ReadLine();
                int cont = 0;

                while (!read.EndOfStream)
                {
                    var line = read.ReadLine();
                    var values = line.Split(';');
                    // The material code needs to be adapted to a format with 18 digits
                    var codigo = GetFormat(values[0]);
                    var valor = "";
                    // Process the material description to delete multiple """
                    if (columna == 3)
                    {
                        // Process the material description to delete multiple """
                        valor = GetNameClean(values[3]);
                    }
                    else
                    {
                        valor = values[columna];
                    }

                    string key = string.Format("{0}{1}{2}", prefijo, codigo.ToLower().Trim(), sufijo);
                    string[] output = new string[] { key, valor };
                    sb.AppendLine(string.Join(delimiter, output));
                    cont++;
                    Console.WriteLine($"Material {cont} convertido correctamente");
                }
                read.Close();
            }
            
            File.WriteAllText(savedPath, sb.ToString());
            
            return savedPath;
        }

        #endregion

        #region CheckUploaded
        /// <summary>
        /// Check if the import has been completed correctly
        /// </summary>
        /// <param name="pathToCheck">Path of the CSV to check</param>
        /// <param name="pathSave">Path to save the generated CSV</param>
        /// <param name="fieldId">Option field ID</param>
        static private void CheckUploaded(string pathToCheck, string pathSave,long fieldId)
        {
            var tf = Api().Tickets.GetTicketFieldById(fieldId).TicketField;

            using (var read = new StreamReader(pathToCheck))
            {
                StringBuilder sb = new StringBuilder();
                while (!read.EndOfStream)
                {
                    var line = read.ReadLine();
                    string codigo = line.Split(';')[0];
                    string valor = line.Split(';')[1];
                    if (valor != "")
                    {
                        bool exists = false;
                        foreach (var cfOpt in tf.CustomFieldOptions)
                        {
                            if (codigo == cfOpt.Value)
                            {
                                exists = true;
                                break;

                            }
                        }
                        // If the data has not been found, add to the error CSV
                        if (!exists)
                        {
                            string[] output = new string[] { codigo, valor };
                            sb.AppendLine(string.Join(";", output));
                        }
                    }
                    
                }
                read.Close();
                File.WriteAllText(pathSave, sb.ToString());
            }
            
        }
        #endregion

        #region UploadDataFormated
        static private void UploadDataFormated(string pathFile, long fieldId)
        {

            using (var reader = new StreamReader(pathFile))
            {

                // Send the product descriptions one by one
                int contUp = 0;

                var tf = Api().Tickets.GetTicketFieldById(fieldId).TicketField;

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    string codigo = line.Split(';')[0];
                    string valor = line.Split(';')[1];
                    if (valor != "")
                    {
                        string outputJSON = "";
                        bool addNew = true;
                        string key = codigo;
                        foreach (var cfOpt in tf.CustomFieldOptions)
                        {

                            if (key == cfOpt.Value)
                            {
                                cfOpt.Name = valor;
                                customFieldOption updateCfOpt = new customFieldOption()
                                {
                                    custom_field_option = cfOpt
                                };

                                outputJSON = Newtonsoft.Json.JsonConvert.SerializeObject(updateCfOpt);
                                addNew = false;
                                break;
                            }

                        }
                        if (addNew)
                        {
                            CustomFieldOptions newCF = new CustomFieldOptions() { Value = key, Name = valor };
                            customFieldOption newCfOpt = new customFieldOption()
                            {
                                custom_field_option = newCF
                            };
                            outputJSON = Newtonsoft.Json.JsonConvert.SerializeObject(newCfOpt);
                        }

                        WebClient wc = new WebClient();
                        wc.Headers.Add(HttpRequestHeader.UserAgent, "Chrome");
                        wc.Encoding = Encoding.UTF8;
                        wc.Headers.Add(HttpRequestHeader.Authorization, "Bearer 8b0f8b026af31f5afbacd073d2b62b755bf5a699d470bd7b8215f0d3fbbd0799");
                        wc.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                        try
                        {
                            wc.UploadString(new Uri($"https://vesteliberia.zendesk.com/api/v2/ticket_fields/{fieldId}/options.json"), "POST", outputJSON);
                        }
                        catch (Exception ex)
                        {
                            SlackHelper.ErrorSlack($"Error al actualizar el campo con id {fieldId}, JSON: {outputJSON}, error: {ex.Message}");
                        }


                        contUp++;
                        Console.WriteLine($"Material {contUp} de 26525 subido correctamente.");
                    }
                    
                }

            }
        }

        #endregion

        /// <summary>
        /// Reads an excel CSV file located in pathFile and updates de product items
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string fileName = "iberia_materials.csv";
            string pathFile = @"C:\Users\vcent\Projects\UpadeVestel\";

            

            // Produc Description
            long fieldId = 360000199358;
            string prefijo = "m_";
            string sufijo = "";
            // Material; Iberia Old Material Number; Model Name; Material Description ;Brand ;Brand Description ;Subgama; Subgama Description
            int columna = 2;



            // Get the CSV file formated
            string convertedPath = GenNewCSV(pathFile + fileName, columna, prefijo, sufijo);
            // string convertedPath = pathFile + $"errors_{columna}.csv";
            // Gets the CSV file and uploads the data of the specified column

            // Be careful with this method
            // UploadDataFormated(convertedPath, fieldId);

            string pathSave = pathFile + $"errors_{columna}.csv";
            // Check if the uploaded materials are the same in the main CSV file
            CheckUploaded(convertedPath, pathSave, fieldId);


        }

            
    
    }
}
