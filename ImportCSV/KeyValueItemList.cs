﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ImportCSV
{
    public class KeyValueItemList
    {
        [JsonProperty("items")]
        public IList<KeyValueItem> Items { get; set; }
    }
}