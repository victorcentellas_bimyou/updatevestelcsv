﻿using System;

namespace ImportCSV.Slack
{
    public class SlackHelper
    {
        public static void NotificarSlack(string urlDestino, string texto)
        {
            SlackClient client = new SlackClient(urlDestino);
            try
            {
                client.PostMessage(texto);
            }
            catch (Exception)
            {
            }
        }

        public static void InformacionSlack(string texto)
        {
            SlackClient client = new SlackClient(string.Empty);
            try
            {
                client.NotifyMessage(texto);
            }
            catch (Exception)
            {
            }
        }

        public static void ErrorSlack(string texto)
        {
            SlackClient client = new SlackClient(string.Empty);
            try
            {
                client.ErrorMessage(texto);
            }
            catch (Exception)
            {
            }
        }
    }
}