﻿using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace ImportCSV.Slack
{
    public class SlackClient
    {
        private Uri _uri;
        private readonly Encoding _encoding = new UTF8Encoding();
        private readonly Uri Notify_Url = new Uri("https://hooks.slack.com/services/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");
        private readonly Uri Error_Url = new Uri("https://hooks.slack.com/services/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");

        public SlackClient(string urlWithAccessToken)
        {
            if (!string.IsNullOrEmpty(urlWithAccessToken))
            {
                _uri = new Uri(urlWithAccessToken);
            }            
        }

        public void NotifyMessage(string text)
        {
            Uri lastURI = _uri;
            _uri = Notify_Url;
            PostMessage(text);
            _uri = lastURI;
        }

        public void ErrorMessage(string text)
        {
            Uri lastURI = _uri;
            _uri = Error_Url;
            PostMessage(text);
            _uri = lastURI;
        }

        //Post a message using simple strings
        public void PostMessage(string text, string username = null, string channel = null, string emoji = null, string icon = null)
        {
            Payload payload = new Payload()
            {
                Channel = channel,
                Username = username,
                Text = text,
                Emoji = emoji,
                Icon = icon
            };

            PostMessage(payload);
        }

        //Post a message using a Payload object
        public void PostMessage(Payload payload)
        {
            string payloadJson = JsonConvert.SerializeObject(payload);
            using (WebClient client = new WebClient())
            {
                var data = new NameValueCollection
                {
                    ["payload"] = payloadJson
                };
                var response = client.UploadValues(_uri, "POST", data);
                //The response text is usually "ok"
                string responseText = _encoding.GetString(response);
            }
        }
    }
}