﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportCSV
{
    public class Item
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class Items
    {
        public List<Item> items { get; set; }
    }

    public class ImportCSVData
    {
        public Items productcode { get; set; }
        public Items brandcode { get; set; }
        public Items subgamacode { get; set; }
        public Items modelname { get; set; }
        public Items productdesc { get; set; }
        public Items branddesc { get; set; }
        public Items subgamadesc { get; set; }
        public Items viboldproductcode { get; set; }




    }

    public class customFieldOption
    {
        public ZendeskApi_v2.Models.Tickets.CustomFieldOptions custom_field_option { get; set; }
    }



}
