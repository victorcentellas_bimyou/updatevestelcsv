﻿using Newtonsoft.Json;

namespace ImportCSV
{
    public class KeyValueItem
    {
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}