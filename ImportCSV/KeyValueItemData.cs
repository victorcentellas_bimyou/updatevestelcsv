﻿using Newtonsoft.Json;

namespace ImportCSV
{
    public class KeyValueItemData
    {
        [JsonProperty("productcode")]
        public KeyValueItemList ProductCode { get; set; }

        [JsonProperty("productdesc")]
        public KeyValueItemList ProductDesc { get; set; }

        [JsonProperty("brandcode")]
        public KeyValueItemList BrandCode { get; set; }

        [JsonProperty("branddesc")]
        public KeyValueItemList BrandDesc { get; set; }

        [JsonProperty("subgamacode")]
        public KeyValueItemList SubgamaCode { get; set; }

        [JsonProperty("subgamadesc")]
        public KeyValueItemList SubgamaDesc { get; set; }

        [JsonProperty("viboldproductcode")]
        public KeyValueItemList VIBOldProductCode { get; set; }

        [JsonProperty("modelname")]
        public KeyValueItemList ModelName { get; set; }
    }
}